All code from: https://github.com/USBGuard/usbguard/tree/usbguard-0.7.4/src/GUI.Qt
This project is licensed as USBGuard is, namely under GPLv2 or later.

Made to compile with newer libusbguard so you can keep using the applet on v0.7.5+
