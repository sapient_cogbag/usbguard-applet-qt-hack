/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionQuit;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *device_tab;
    QGridLayout *gridLayout_2;
    QPushButton *reset_button;
    QSpacerItem *horizontalSpacer;
    QPushButton *apply_button;
    QTreeView *device_view;
    QCheckBox *permanent_checkbox;
    QWidget *message_tab;
    QGridLayout *gridLayout_3;
    QTextBrowser *messages_text;
    QWidget *settings_tab;
    QGridLayout *gridLayout_4;
    QToolBox *toolBox;
    QWidget *page;
    QGridLayout *gridLayout_6;
    QCheckBox *notify_allowed;
    QCheckBox *notify_removed;
    QCheckBox *notify_rejected;
    QCheckBox *notify_inserted;
    QSpacerItem *verticalSpacer_2;
    QCheckBox *notify_blocked;
    QCheckBox *notify_ipc;
    QCheckBox *notify_present;
    QWidget *page_2;
    QGridLayout *gridLayout_5;
    QCheckBox *decision_permanent_checkbox;
    QCheckBox *show_reject_button_checkbox;
    QSpacerItem *verticalSpacer;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label;
    QComboBox *default_decision_combobox;
    QSpinBox *decision_timeout;
    QComboBox *decision_method_combobox;
    QCheckBox *randomize_position_checkbox;
    QCheckBox *mask_serial_checkbox;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(527, 391);
        actionQuit = new QAction(MainWindow);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        device_tab = new QWidget();
        device_tab->setObjectName(QString::fromUtf8("device_tab"));
        device_tab->setEnabled(true);
        gridLayout_2 = new QGridLayout(device_tab);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        reset_button = new QPushButton(device_tab);
        reset_button->setObjectName(QString::fromUtf8("reset_button"));

        gridLayout_2->addWidget(reset_button, 1, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 1, 1, 1, 1);

        apply_button = new QPushButton(device_tab);
        apply_button->setObjectName(QString::fromUtf8("apply_button"));
        apply_button->setEnabled(true);
        apply_button->setCheckable(false);

        gridLayout_2->addWidget(apply_button, 1, 3, 1, 1);

        device_view = new QTreeView(device_tab);
        device_view->setObjectName(QString::fromUtf8("device_view"));
        device_view->setEnabled(false);
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Sans Mono"));
        device_view->setFont(font);
        device_view->setStyleSheet(QString::fromUtf8(""));
        device_view->setFrameShape(QFrame::StyledPanel);
        device_view->setFrameShadow(QFrame::Plain);
        device_view->setEditTriggers(QAbstractItemView::CurrentChanged|QAbstractItemView::EditKeyPressed|QAbstractItemView::SelectedClicked);
        device_view->setAlternatingRowColors(true);
        device_view->setSelectionBehavior(QAbstractItemView::SelectRows);
        device_view->setTextElideMode(Qt::ElideMiddle);
        device_view->setIndentation(20);
        device_view->setRootIsDecorated(true);
        device_view->setUniformRowHeights(true);
        device_view->setSortingEnabled(false);
        device_view->header()->setProperty("showSortIndicator", QVariant(false));
        device_view->header()->setStretchLastSection(true);

        gridLayout_2->addWidget(device_view, 0, 0, 1, 4);

        permanent_checkbox = new QCheckBox(device_tab);
        permanent_checkbox->setObjectName(QString::fromUtf8("permanent_checkbox"));

        gridLayout_2->addWidget(permanent_checkbox, 1, 2, 1, 1);

        tabWidget->addTab(device_tab, QString());
        message_tab = new QWidget();
        message_tab->setObjectName(QString::fromUtf8("message_tab"));
        gridLayout_3 = new QGridLayout(message_tab);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        messages_text = new QTextBrowser(message_tab);
        messages_text->setObjectName(QString::fromUtf8("messages_text"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Monospace"));
        font1.setPointSize(8);
        messages_text->setFont(font1);

        gridLayout_3->addWidget(messages_text, 0, 0, 1, 1);

        tabWidget->addTab(message_tab, QString());
        settings_tab = new QWidget();
        settings_tab->setObjectName(QString::fromUtf8("settings_tab"));
        gridLayout_4 = new QGridLayout(settings_tab);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        toolBox = new QToolBox(settings_tab);
        toolBox->setObjectName(QString::fromUtf8("toolBox"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        page->setGeometry(QRect(0, 0, 198, 126));
        gridLayout_6 = new QGridLayout(page);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        notify_allowed = new QCheckBox(page);
        notify_allowed->setObjectName(QString::fromUtf8("notify_allowed"));

        gridLayout_6->addWidget(notify_allowed, 1, 0, 1, 1);

        notify_removed = new QCheckBox(page);
        notify_removed->setObjectName(QString::fromUtf8("notify_removed"));

        gridLayout_6->addWidget(notify_removed, 0, 2, 1, 1);

        notify_rejected = new QCheckBox(page);
        notify_rejected->setObjectName(QString::fromUtf8("notify_rejected"));
        notify_rejected->setChecked(true);

        gridLayout_6->addWidget(notify_rejected, 2, 0, 1, 1);

        notify_inserted = new QCheckBox(page);
        notify_inserted->setObjectName(QString::fromUtf8("notify_inserted"));
        notify_inserted->setChecked(true);

        gridLayout_6->addWidget(notify_inserted, 0, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer_2, 4, 0, 1, 1);

        notify_blocked = new QCheckBox(page);
        notify_blocked->setObjectName(QString::fromUtf8("notify_blocked"));
        notify_blocked->setChecked(true);

        gridLayout_6->addWidget(notify_blocked, 1, 2, 1, 1);

        notify_ipc = new QCheckBox(page);
        notify_ipc->setObjectName(QString::fromUtf8("notify_ipc"));

        gridLayout_6->addWidget(notify_ipc, 3, 0, 1, 1);

        notify_present = new QCheckBox(page);
        notify_present->setObjectName(QString::fromUtf8("notify_present"));

        gridLayout_6->addWidget(notify_present, 2, 2, 1, 1);

        toolBox->addItem(page, QString::fromUtf8("Notifications"));
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        page_2->setGeometry(QRect(0, 0, 487, 246));
        gridLayout_5 = new QGridLayout(page_2);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        decision_permanent_checkbox = new QCheckBox(page_2);
        decision_permanent_checkbox->setObjectName(QString::fromUtf8("decision_permanent_checkbox"));

        gridLayout_5->addWidget(decision_permanent_checkbox, 4, 0, 1, 3);

        show_reject_button_checkbox = new QCheckBox(page_2);
        show_reject_button_checkbox->setObjectName(QString::fromUtf8("show_reject_button_checkbox"));

        gridLayout_5->addWidget(show_reject_button_checkbox, 5, 0, 1, 3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_5->addItem(verticalSpacer, 8, 0, 1, 1);

        label_2 = new QLabel(page_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_5->addWidget(label_2, 1, 0, 1, 1);

        label_3 = new QLabel(page_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_5->addWidget(label_3, 3, 0, 1, 1);

        label = new QLabel(page_2);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_5->addWidget(label, 0, 0, 1, 1);

        default_decision_combobox = new QComboBox(page_2);
        default_decision_combobox->addItem(QString());
        default_decision_combobox->addItem(QString());
        default_decision_combobox->addItem(QString());
        default_decision_combobox->setObjectName(QString::fromUtf8("default_decision_combobox"));

        gridLayout_5->addWidget(default_decision_combobox, 0, 2, 1, 1);

        decision_timeout = new QSpinBox(page_2);
        decision_timeout->setObjectName(QString::fromUtf8("decision_timeout"));
        decision_timeout->setMaximum(3600);
        decision_timeout->setValue(23);

        gridLayout_5->addWidget(decision_timeout, 1, 2, 1, 1);

        decision_method_combobox = new QComboBox(page_2);
        decision_method_combobox->addItem(QString());
        decision_method_combobox->setObjectName(QString::fromUtf8("decision_method_combobox"));

        gridLayout_5->addWidget(decision_method_combobox, 3, 2, 1, 1);

        randomize_position_checkbox = new QCheckBox(page_2);
        randomize_position_checkbox->setObjectName(QString::fromUtf8("randomize_position_checkbox"));
        randomize_position_checkbox->setChecked(true);

        gridLayout_5->addWidget(randomize_position_checkbox, 6, 0, 1, 1);

        mask_serial_checkbox = new QCheckBox(page_2);
        mask_serial_checkbox->setObjectName(QString::fromUtf8("mask_serial_checkbox"));
        mask_serial_checkbox->setChecked(true);

        gridLayout_5->addWidget(mask_serial_checkbox, 7, 0, 1, 1);

        toolBox->addItem(page_2, QString::fromUtf8("Device Dialog"));

        gridLayout_4->addWidget(toolBox, 0, 0, 1, 2);

        tabWidget->addTab(settings_tab, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);
        toolBox->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionQuit->setText(QCoreApplication::translate("MainWindow", "Quit", nullptr));
        reset_button->setText(QCoreApplication::translate("MainWindow", "Reset", nullptr));
        apply_button->setText(QCoreApplication::translate("MainWindow", "Apply", nullptr));
        permanent_checkbox->setText(QCoreApplication::translate("MainWindow", "Permanently", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(device_tab), QCoreApplication::translate("MainWindow", "Devices", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(message_tab), QCoreApplication::translate("MainWindow", "Messages", nullptr));
        notify_allowed->setText(QCoreApplication::translate("MainWindow", "Allowed", nullptr));
        notify_removed->setText(QCoreApplication::translate("MainWindow", "Removed", nullptr));
        notify_rejected->setText(QCoreApplication::translate("MainWindow", "Rejected", nullptr));
        notify_inserted->setText(QCoreApplication::translate("MainWindow", "Inserted", nullptr));
        notify_blocked->setText(QCoreApplication::translate("MainWindow", "Blocked", nullptr));
        notify_ipc->setText(QCoreApplication::translate("MainWindow", "IPC status", nullptr));
        notify_present->setText(QCoreApplication::translate("MainWindow", "Present", nullptr));
        toolBox->setItemText(toolBox->indexOf(page), QCoreApplication::translate("MainWindow", "Notifications", nullptr));
        decision_permanent_checkbox->setText(QCoreApplication::translate("MainWindow", "Decision is permanent by default", nullptr));
#if QT_CONFIG(tooltip)
        show_reject_button_checkbox->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p>Reject causes the device to be logically removed from the operating system. The device might still get power from the from the bus, however, it has to be reinserted for it to get visible for the operating system again.</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        show_reject_button_checkbox->setText(QCoreApplication::translate("MainWindow", "Show the Reject button", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Default decision timeout (seconds)", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Decision method", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Default decision", nullptr));
        default_decision_combobox->setItemText(0, QCoreApplication::translate("MainWindow", "allow", nullptr));
        default_decision_combobox->setItemText(1, QCoreApplication::translate("MainWindow", "block", nullptr));
        default_decision_combobox->setItemText(2, QCoreApplication::translate("MainWindow", "reject", nullptr));

        decision_timeout->setSuffix(QString());
        decision_method_combobox->setItemText(0, QCoreApplication::translate("MainWindow", "Buttons", nullptr));

        randomize_position_checkbox->setText(QCoreApplication::translate("MainWindow", "Randomize position of the window", nullptr));
#if QT_CONFIG(tooltip)
        mask_serial_checkbox->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p>Starting from the end, mask every 2nd character in the displayed serial number value with an asterisk symbol.</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        mask_serial_checkbox->setText(QCoreApplication::translate("MainWindow", "Mask the serial number value", nullptr));
        toolBox->setItemText(toolBox->indexOf(page_2), QCoreApplication::translate("MainWindow", "Device Dialog", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(settings_tab), QCoreApplication::translate("MainWindow", "Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MAINWINDOW_H
