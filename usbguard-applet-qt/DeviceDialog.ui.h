/********************************************************************************
** Form generated from reading UI file 'DeviceDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef DEVICEDIALOG_H
#define DEVICEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DeviceDialog
{
public:
    QGridLayout *gridLayout;
    QFrame *frame;
    QGridLayout *gridLayout_3;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QLabel *name_label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *serial_label;
    QLabel *deviceid_label;
    QListWidget *interface_list;
    QCheckBox *permanent_checkbox;
    QWidget *widget;
    QGridLayout *gridLayout_2;
    QPushButton *block_button;
    QPushButton *allow_button;
    QPushButton *reject_button;
    QLabel *hint_label;

    void setupUi(QDialog *DeviceDialog)
    {
        if (DeviceDialog->objectName().isEmpty())
            DeviceDialog->setObjectName(QString::fromUtf8("DeviceDialog"));
        DeviceDialog->resize(423, 386);
        DeviceDialog->setMinimumSize(QSize(300, 300));
        gridLayout = new QGridLayout(DeviceDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame = new QFrame(DeviceDialog);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("Monospace"));
        font.setPointSize(12);
        frame->setFont(font);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_3 = new QGridLayout(frame);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setPointSize(10);
        label->setFont(font1);
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(label, 1, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 1, 2, 1, 1);

        name_label = new QLabel(frame);
        name_label->setObjectName(QString::fromUtf8("name_label"));
        QFont font2;
        font2.setBold(true);
        font2.setWeight(75);
        name_label->setFont(font2);

        gridLayout_3->addWidget(name_label, 1, 1, 1, 1);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font1);
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_2, 0, 0, 1, 1);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_3, 2, 0, 1, 1);

        serial_label = new QLabel(frame);
        serial_label->setObjectName(QString::fromUtf8("serial_label"));
        serial_label->setFont(font2);

        gridLayout_3->addWidget(serial_label, 2, 1, 1, 1);

        deviceid_label = new QLabel(frame);
        deviceid_label->setObjectName(QString::fromUtf8("deviceid_label"));
        deviceid_label->setFont(font2);

        gridLayout_3->addWidget(deviceid_label, 0, 1, 1, 1);

        interface_list = new QListWidget(frame);
        interface_list->setObjectName(QString::fromUtf8("interface_list"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(interface_list->sizePolicy().hasHeightForWidth());
        interface_list->setSizePolicy(sizePolicy1);
        interface_list->setMaximumSize(QSize(16777215, 128));
        interface_list->setFont(font1);
        interface_list->setFrameShape(QFrame::StyledPanel);
        interface_list->setEditTriggers(QAbstractItemView::DoubleClicked);
        interface_list->setProperty("showDropIndicator", QVariant(false));
        interface_list->setAlternatingRowColors(true);

        gridLayout_3->addWidget(interface_list, 3, 0, 1, 3);


        gridLayout->addWidget(frame, 0, 0, 1, 3);

        permanent_checkbox = new QCheckBox(DeviceDialog);
        permanent_checkbox->setObjectName(QString::fromUtf8("permanent_checkbox"));

        gridLayout->addWidget(permanent_checkbox, 2, 0, 1, 2);

        widget = new QWidget(DeviceDialog);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy2);
        gridLayout_2 = new QGridLayout(widget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        block_button = new QPushButton(widget);
        block_button->setObjectName(QString::fromUtf8("block_button"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(block_button->sizePolicy().hasHeightForWidth());
        block_button->setSizePolicy(sizePolicy3);
        QFont font3;
        font3.setPointSize(14);
        font3.setBold(true);
        font3.setWeight(75);
        block_button->setFont(font3);
        block_button->setAutoFillBackground(false);
        block_button->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 80, 0)\n"
""));

        gridLayout_2->addWidget(block_button, 0, 1, 1, 1);

        allow_button = new QPushButton(widget);
        allow_button->setObjectName(QString::fromUtf8("allow_button"));
        sizePolicy3.setHeightForWidth(allow_button->sizePolicy().hasHeightForWidth());
        allow_button->setSizePolicy(sizePolicy3);
        allow_button->setFont(font3);
        allow_button->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 128, 0)"));

        gridLayout_2->addWidget(allow_button, 0, 0, 1, 1);

        reject_button = new QPushButton(widget);
        reject_button->setObjectName(QString::fromUtf8("reject_button"));
        sizePolicy3.setHeightForWidth(reject_button->sizePolicy().hasHeightForWidth());
        reject_button->setSizePolicy(sizePolicy3);
        reject_button->setFont(font3);
        reject_button->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 0, 0)"));

        gridLayout_2->addWidget(reject_button, 0, 2, 1, 1);

        hint_label = new QLabel(widget);
        hint_label->setObjectName(QString::fromUtf8("hint_label"));
        QFont font4;
        font4.setPointSize(8);
        hint_label->setFont(font4);
        hint_label->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(hint_label, 1, 0, 1, 3);

        allow_button->raise();
        reject_button->raise();
        block_button->raise();
        hint_label->raise();

        gridLayout->addWidget(widget, 1, 0, 1, 3);


        retranslateUi(DeviceDialog);

        QMetaObject::connectSlotsByName(DeviceDialog);
    } // setupUi

    void retranslateUi(QDialog *DeviceDialog)
    {
        DeviceDialog->setWindowTitle(QCoreApplication::translate("DeviceDialog", "USBGuard Device Dialog", nullptr));
        label->setText(QCoreApplication::translate("DeviceDialog", "Name:", nullptr));
        name_label->setText(QCoreApplication::translate("DeviceDialog", "TextLabel", nullptr));
        label_2->setText(QCoreApplication::translate("DeviceDialog", "Device ID:", nullptr));
        label_3->setText(QCoreApplication::translate("DeviceDialog", "Serial #:", nullptr));
        serial_label->setText(QCoreApplication::translate("DeviceDialog", "TextLabel", nullptr));
        deviceid_label->setText(QCoreApplication::translate("DeviceDialog", "TextLabel", nullptr));
        permanent_checkbox->setText(QCoreApplication::translate("DeviceDialog", "Make the decision permanent", nullptr));
        block_button->setText(QCoreApplication::translate("DeviceDialog", "Block", nullptr));
        allow_button->setText(QCoreApplication::translate("DeviceDialog", "Allow", nullptr));
        reject_button->setText(QCoreApplication::translate("DeviceDialog", "Reject", nullptr));
        hint_label->setText(QCoreApplication::translate("DeviceDialog", "(Press Escape to stop the countdown)", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DeviceDialog: public Ui_DeviceDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // DEVICEDIALOG_H
